<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'akoba' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p&RJB5pF$y)EM}/PCs6sy5U:Z-|1U0nJDgc1guvu|US]mRf7EJWU}.-rS%G)dJZY' );
define( 'SECURE_AUTH_KEY',  'k/cl$DNt$tna-c:Yj7Q>J[<X}k]DkRh|Q}8&D3B.-xGtBLwacOs3yO}y)2[n@0%%' );
define( 'LOGGED_IN_KEY',    'Z| 8POkW7i.ck#wc-lM{B1]7uw[Q{U=4~T}4fzJ7?:k?N_Gy1BOsQ?=r%sQ:<6X!' );
define( 'NONCE_KEY',        '{4T.GRnyFs e9@@hKk$*FmWvT01> }rJ0pu>mRf<^[XxPjy|vpZV^tJDSI&#^@xX' );
define( 'AUTH_SALT',        'Q7n>wwC4:0[|}PS?~@ek&2^PVYj[H N`.=&^)mx3w^BEGGI@_$;#~1CTF%lb9S0?' );
define( 'SECURE_AUTH_SALT', '9^z,5E:yR _ynl$s]Mn)_IQLCf2/y*Ax1j;Kv>4<GndE~R]{-#jZqfj!mni!}m,]' );
define( 'LOGGED_IN_SALT',   '[J,BA ekP7l#)*x7[.uU:{GEOw~cYW8FflBfmh}yXFXc`xUkN.|J()xe}{04 bx.' );
define( 'NONCE_SALT',       '}`Z+X^b|ngvY,UBFmoq0W_WvQ-i#5B~=$n|jU-urfa>JNiBskg^oZ->iLWQ`8ru)' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
