<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Templateheld
*/

?>
<footer id="footer" data-section-name="footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col text-center">
        <a class="logo" href="#start">
  				<img src="<?php echo get_template_directory_uri(); ?>/img/akoba-logo-footer.svg" alt="AKOBA">
  			</a>
      </div>
      <div class="col-12 col-md socialmedia-bar">
        <a target="_blank" href="https://www.instagram.com/akobafoods/"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" alt="Instagram"></a>
        <a target="_blank" href="https://www.facebook.com/Akoba-Foods-407163009725439/"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg" alt="Facebook"></a>
      </div>
      <address class="col-12 col-md">
        <h2>Kontakt</h2>
        <p>Adresse<br>AKOBA-Foods GmbH<br>Affinger Straße 4<br>86167 Augsburg</p>
        <p>Telefon<br><a class="tel" href="tel:082145071871">0821 450 71 871</a></p>
      </address>
      <div class="col-6 col-md">
        <h2>Unternehmen</h2>
        <div><a href="#about">Über uns</a></div>
      </div>
      <div class="col-6 col-md">
        <h2>Datenschutz</h2>
        <div><a data-toggle="modal" data-target="#imprint" href="#">Impressum</a></div>
        <div><a data-toggle="modal" data-target="#privacy" href="#">Datenschutz</a></div>
      </div>
    </div>
  </div>
</footer>

<div id="imprint" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Impressum</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p><strong>Angaben gemäß § 5 TMG</strong><br>

        <p>AKOBA Foods GmbH<br>
        Affinger Straße 4<br>
        86167 Augsburg</p>

        <strong><p>Vertreten durch</p></strong>
        <p>Herr V. Özbakir</p>

        <strong><p>Kontakt</p></strong>
        <p>Telefon: 0821 / 450 71 871<br>
        E-Mail: info@akoba-foods.com<br>
        Internet: www.akoba-foods.com</p>

        <strong><p>Registereintrag</p></strong>
        <p>Eintragung im Handelsregister<br>
        Registernummer: HRB 32264<br>
        Registergericht: Augsburg</p>
      </div>
    </div>
  </div>
</div>

<div id="privacy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Datenschutz</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>Wir nehmen den Schutz Ihrer persönlichen Daten sehr ernst und halten uns strikt an die gesetzlichen und behördlichen Datenschutzbestimmungen. Personenbezogene Daten werden auf dieser Webseite nur im technisch und organisatorisch notwendigen Umfang erhoben. In keinem Fall werden die erhobenen Daten verkauft. Die nachfolgende Erklärung gibt Ihnen einen Überblick darüber, wie wir diesen Schutz gewährleisten und welche Art von Daten zu welchem Zweck erhoben werden.</p>
          <p><strong>§1 Haftungsbeschränkung, kein Vertragsverhältnis</strong></p>
          <p>Die Inhalte dieser Website werden mit größtmöglicher Sorgfalt erstellt. Der Anbieter übernimmt jedoch keine Gewähr für die Richtigkeit, Vollständigkeit und Aktualität der bereitgestellten Inhalte. Namentlich gekennzeichnete Beiträge geben die Meinung des jeweiligen Autors und nicht immer die Meinung des Anbieters wieder. Mit der reinen Nutzung der Website des Anbieters kommt keinerlei Vertragsverhältnis zwischen dem Nutzer und dem Anbieter zustande. Insofern ergeben sich auch keinerlei vertragliche oder quasivertragliche Ansprüche gegen den Anbieter.</p>
          <p>Für den Fall, dass die Nutzung der Website doch zu einem Vertragsverhältnis führen sollte, gilt rein vorsorglich nachfolgende Haftungsbeschränkung: Der Anbieter haftet für Vorsatz und grobe Fahrlässigkeit sowie bei Verletzung einer wesentlichen Vertragspflicht (Kardinalpflicht). Der Anbieter haftet unter Begrenzung auf Ersatz des bei Vertragsschluss vorhersehbaren vertragstypischen Schadens für solche Schäden, die auf einer leicht fahrlässigen Verletzung von Kardinalpflichten durch ihn oder eines seiner gesetzlichen Vertreter oder Erfüllungsgehilfen beruhen. Bei leicht fahrlässiger Verletzung von Nebenpflichten, die keine Kardinalpflichten sind, haftet der Anbieter nicht. Die Haftung für Schäden, die in den Schutzbereich einer vom Anbieter gegebenen Garantie oder Zusicherung fallen sowie die Haftung für Ansprüche aufgrund des Produkthaftungsgesetzes und Schäden aus der Verletzung des Lebens, des Körpers oder der Gesundheit bleibt hiervon unberührt.</p>
          <p><strong>§2 Externe Links</strong></p>
          <p>Diese Website enthält Verknüpfungen zu Websites Dritter („externe Links“). Diese Websites unterliegen der Haftung des jeweiligen Betreibers. Der Anbieter hat bei der erstmaligen Verknüpfung der externen Links die fremden Inhalte daraufhin überprüft, ob etwaige Rechtsverstöße bestehen. Zu dem Zeitpunkt waren keine Rechtsverstöße ersichtlich. Der Anbieter hat keinerlei Einfluss auf die aktuelle und zukünftige Gestaltung und auf die Inhalte der verknüpften Seiten. Das Setzen von externen Links bedeutet nicht, dass sich der Anbieter die hinter dem Verweis oder Link liegenden Inhalte zu eigen macht. Eine ständige Kontrolle der externen Links ist für den Anbieter ohne konkrete Hinweise auf Rechtsverstöße nicht zumutbar. Bei Kenntnis von Rechtsverstößen werden jedoch derartige externe Links unverzüglich gelöscht.<br>
              Mit Urteil vom 12. Mai 1998 – 312 O 85/98 – “Haftung für Links” hat das Landgericht (LG) Hamburg entschieden, dass man durch die Anbringung eines Links, die Inhalte der gelinkten Seite ggf. mit zu verantworten hat. Dies kann – so das LG – nur dadurch verhindert werden, dass man sich ausdrücklich von diesen Inhalten distanziert. Hiermit distanzieren wir uns ausdrücklich von allen Inhalten aller gelinkten Seiten auf dieser Seite. Diese Erklärung gilt für alle auf unserer Homepage angebrachten Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.</p>
          <p><strong>§3 Verfügbarkeit der Website</strong></p>
          <p>Der Anbieter wird sich bemühen, den Dienst möglichst unterbrechungsfrei zum Abruf anzubieten. Auch bei aller Sorgfalt können aber Ausfallzeiten nicht ausgeschlossen werden. Der Anbieter behält sich das Recht vor, sein Angebot jederzeit zu ändern oder einzustellen.</p>
          <p><strong>§4 Urheber- und Leistungsschutzrechte</strong></p>
          <p>Die auf dieser Website veröffentlichten Inhalte unterliegen dem deutschen Urheber- und Leistungsschutzrecht. Jede vom deutschen Urheber- und Leistungsschutzrecht nicht zugelassene Verwertung bedarf der vorherigen schriftlichen Zustimmung des Anbieters oder jeweiligen Rechteinhabers. Dies gilt insbesondere für Vervielfältigung, Bearbeitung, Übersetzung, Einspeicherung, Verarbeitung bzw. Wiedergabe von Inhalten in Datenbanken oder anderen elektronischen Medien und Systemen. Inhalte und Rechte Dritter sind dabei als solche gekennzeichnet. Die unerlaubte Vervielfältigung oder Weitergabe einzelner Inhalte oder kompletter Seiten ist nicht gestattet und strafbar. Lediglich die Herstellung von Kopien und Downloads für den persönlichen, privaten und nicht kommerziellen Gebrauch ist erlaubt.<br>
              Links zur Website des Anbieters sind jederzeit willkommen und bedürfen keiner Zustimmung durch den Anbieter der Website. Die Darstellung dieser Website in fremden Frames ist nur mit Erlaubnis zulässig.</p>
          <p><strong>§5 Keine Abmahnung ohne vorherige Kontaktaufnahme</strong></p>
          <p>Sollte der Inhalt oder die Aufmachung dieser Seiten fremde Rechte Dritter oder gesetzliche Bestimmungen verletzen, so wird um eine entsprechende Nachricht ohne Kostennote gebeten. Zur Vermeidung unnötiger Rechtsstreite und Kosten bitten wir Sie im Falle von wettbewerbs-, urheber-, datenschutzrechtlichen oder sonstigen Problemen, schon im Vorfeld mit uns Kontakt aufzunehmen. Wir garantieren Ihnen, zu Recht beanstandete Passagen oder Verfahren unverzüglich zu beseitigen, ohne dass Sie einen Rechtsbeistand einschalten müssen. Die Beseitigung einer möglicherweise von diesen Seiten ausgehenden Schutzrecht-Verletzung durch Schutzrecht-Inhaber selbst darf nicht ohne Zustimmung stattfinden. Zugleich weisen wir darauf hin, dass wir andernfalls Kostennoten von Rechtsbeiständen unter Hinweis auf Ihre Schadensminderungspflicht als unbegründet zurückweisen und gegebenenfalls Gegenklage wegen Verletzung vorgenannter Bestimmungen erheben werden.</p>
          <p><strong>§6 Datenverarbeitung auf dieser Internetseite</strong></p>
          <p>Unser Server erhebt und speichert automatisch in Log Files Informationen, die Ihr Browser an uns übermittelt. Dies sind:</p>
          <ul>
              <li>Browsertyp/ -version</li>
              <li>verwendetes Betriebssystem</li>
              <li>Referrer URL (die zuvor besuchte Seite)</li>
              <li>Hostname des zugreifenden Rechners (IP Adresse)</li>
              <li>Uhrzeit der Serveranfrage.</li>
          </ul>
          <p>Diese Daten sind für uns nicht bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen.</p>
          <p><strong>§7 Cookies</strong></p>
          <p>Diese Webseite verwendet keine Cookies. Es werden keine Cookies erstellt und Informationen in Cookies abgespeichert.</p>
          <p><strong>§8 Newsletter</strong></p>
          <p>Wenn Sie den auf der Webseite angebotenen Newsletter empfangen möchten, benötigen wir von Ihnen eine valide Email-Adresse sowie Informationen, die uns die Überprüfung gestatten, dass Sie der Inhaber der angegebenen Email-Adresse sind bzw. deren Inhaber mit dem Empfang des Newsletters einverstanden ist. Weitere Daten werden nicht erhoben.<br>
              Ihre Einwilligung zur Speicherung der Daten, der Email-Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen.</p>
          <p><strong>§9 Auskunftsrecht</strong></p>
          <p>Sie haben jederzeit das Recht auf Auskunft über die bezüglich Ihrer Person gespeicherten Daten, deren Herkunft und Empfänger sowie den Zweck der Speicherung. Auskunft über die gespeicherten Daten geben unsere Datenschutzbeauftragten.</p>
          <p><strong>§10 Weitere Informationen</strong></p>
          <p>Ihr Vertrauen ist uns wichtig. Daher möchten wir Ihnen jederzeit Rede und Antwort bezüglich der Verarbeitung Ihrer personenbezogenen Daten stehen. Wenn Sie Fragen haben, die Ihnen diese Datenschutzerklärung nicht beantworten konnte oder wenn Sie zu einem Punkt vertiefte Informationen wünschen, wenden Sie sich an uns.</p>
          <p><strong>§11 Anwendbares Recht</strong></p>
          <p>Es gilt ausschließlich das maßgebliche Recht der Bundesrepublik Deutschland.</p>
      </div>
      </div>
    </div>
  </div>
</div>

<aside id="mobileNav" class="col-8 col-sm-4 col-md-3"></aside>

<script src="<?php echo get_template_directory_uri(); ?>/js/min/jquery-slim.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/min/jquery-ui.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/min/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/min/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/min/main.min.js"></script>

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/cookieconsent.min.js"></script>
<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "rgba(0, 0, 0, 0.67)"
                },
                "button": {
                    "background": "#dc3545"
                }
            },
            "content": {
                "message": "Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies setzen.",
                "dismiss": "Verstanden!",
                "link": "Weitere Informationen zu Cookies und Datenschutz finden Sie hier.",
                "href": ""
            }
        })});
</script>
</body>
</html>
