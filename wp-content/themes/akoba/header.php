<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Templateheld
*/

?>
<!doctype html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>AKOBA Foods</title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<button class="navbar-toggler d-lg-none" type="button" data-toggle="toggle" data-target="#mobileNav" aria-controls="mobileNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<nav id="header">
		<div class="col">
			<div id="nav-left">
				<ul class="nav mx-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#start"><span>Startseite</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#about"><span>Akoba</span></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-auto">
			<a class="navbar-brand mx-auto logo nav-link" href="#start">
				<img src="<?php echo get_template_directory_uri(); ?>/img/akoba-logo.svg" alt="AKOBA">
			</a>
		</div>
		<div class="col">
			<div id="nav-right">
				<ul class="nav mx-auto">
					<li class="nav-item">
						<a class="nav-link" href="#sauce"><span>Unsere Sosse</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#inspiration"><span>Inspiration</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#footer"><span>Kontakt</span></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
