<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Templateheld
*/

get_header(); ?>
<section id="intro">
  <div class="intro-wrapper text-center">
    <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-logo.svg" alt="AKOBA">
    <h1>Nat&uuml;rlich die Natur schmecken.</h1>
  </div>
</section>
<section id="start" data-section-name="start">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <figure class="carousel-item active">
        <img src="<?php echo get_template_directory_uri(); ?>/img/mainpicture.jpg" alt="Slider 1">
        <figcaption class="carousel-caption">
          <h1>AKOBA</h1>
          <p>natural. handmade. vegan.</p>
        </figcaption>
      </figure>
        <!--<figure class="carousel-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/slider2.png" alt="Slider 2">
        <figcaption class="carousel-caption">
          <h1>AKOBA</h1>
          <p>natural-handmade-vegan</p>
        </figcaption>
      </figure>  -->
    </div>
   <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Zur&uuml;ck</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Nächstes</span>
    </a> -->
  </div>
</section>

<section id="about" data-section-name="about">
  <div class="container">
    <h2 class="text-center">Akoba - Scharf. Pur. Natur.</h2>
    <div class="row">
      <div class="col-md-4">
        <h3>Handgemacht</h3>
          <img src="<?php echo get_template_directory_uri(); ?>/img/handwithhabaneros.jpg" alt="Habanero in Hand">
          <p>Unsere Saucen werden frisch und von Hand hergestellt, jeden Tag.
          <br>AKOBA Saucen werden in R&auml;umen hergestellt, die mehr an große K&uuml;chen als an Produktionshallen erinnern.
          <br>Wir produzieren immer mit einem pers&ouml;nlichen Touch, schaffen dabei Arbeitspl&auml;tze und nutzen die Ressourcen, die wir vor Ort finden.</p>
      </div>

        <div class="col-md-4">
            <h3>Frische</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/img/freshhabaneros.jpg" alt="Chilifeld">
            <p>Frische Zutaten sind f&uuml;r uns das Wichtigste – sie sind das Herz und Geschmack unserer Saucen.
            <br>Wir sind der &Uuml;berzeugung, dass Kunden das Recht haben, genau zu erfahren, was unsere Saucen besonders macht: nat&uuml;rliche und frische Zutaten.
            <br>Unsere Saucen f&uuml;r Veganer geeignet – diese Information ist klar auf unseren Produkten zu erkennen.</p>
        </div>
        <div class="col-md-4">
        <h3>Geschmack</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/img/steakinspiration.png" alt="Steak">
            <p>Unsere Saucen verfeinern den Geschmack und holen das Beste aus Ihren Gerichten raus.
            <br>Einer unserer Grunds&auml;tze ist es, den nat&uuml;rlichen Geschmack der Gerichte nicht zu &uuml;bert&uuml;nchen – AKOBA Saucen geben Ihren Gerichten die fehlende Sch&auml;rfe und das nat&uuml;rliche Aroma unserer Zutaten.
            <br>Die feine Erg&auml;nzung zum perfekten Geschmack.</p>
      </div>
    </div>
  </div>
</section>

<section id="sauce" data-section-name="sauce">
  <div class="container">
    <h2 class="text-center">Unsere Sosse</h2>
    <div id="sauce-triggers">
      <button data-target="#sauce-habanero" type="button" class="active">Hot Habanero Sauce</button>
      <button data-target="#sauce-barbecue" type="button">Hot Barbecue Sauce</button>
      <button data-target="#sauce-curry" type="button">Hot Curry Sauce</button>
      <button data-target="#sauce-trinidad" type="button">Trinidad Scorpion Sauce</button>
    </div>
    <div id="sauce-targets" class="row">
      <div id="sauce-habanero" class="col-12 sauce-target active">
        <div class="row">
            <div class="col-12 sauce-ingredients order-2 order-md-1">
                <div class="sauce-ingredients-container pos-3 vinegar">
                    <img src="<?php echo get_template_directory_uri(); ?>/svg/essig.svg" alt="Essig">
                    <p>Essig</p>
                </div>
                <!--<div class="sauce-ingredients-container pos-2-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/gewuerznelken.svg" alt="Gewürznelken">
              <p>Gewürznelken</p>
            </div>-->
                <div class="sauce-ingredients-container pos-1-1 habanero">
                    <img src="<?php echo get_template_directory_uri(); ?>/svg/habanero-1.svg" alt="Habanero">
                    <p>Habanero</p>
                </div>
                <!--<div class="sauce-ingredients-container pos-1-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/kurkuma.svg" alt="Kurkuma">
              <p>Kurkuma</p>
            </div>-->
                <div class="sauce-ingredients-container pos-4 oil">
                    <img src="<?php echo get_template_directory_uri(); ?>/svg/oel.svg" alt="Öl">
                    <p>Öl</p>
                </div>
                <div class="sauce-ingredients-container pos-2-1 tomato">
                    <img src="<?php echo get_template_directory_uri(); ?>/svg/tomate.svg" alt="Tomate">
                    <p>Tomate</p>
                </div>
                <!--<div class="sauce-ingredients-container pos-3 zimt">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zimt.svg" alt="Zimt">
              <p>Zimt</p>
            </div>-->
                <!--<div class="sauce-ingredients-container pos-3-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zwiebel.svg" alt="Zwiebel">
              <p>Zwiebel</p>
            </div>-->
            </div>
          <div class="col-12 sauce-images">
            <div class="row">
              <div class="col-5 ml-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-habanero.png" alt="Hot Habanero Sauce">
              </div>
              <div class="col-5 mr-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-habanero-klein.png" alt="Hot Habanero Sauce">
              </div>
            </div>
          </div>
          <div class="col-12 sauce-details order-3 order-md-2">
            <div class="row">
              <div class="col-12 col-md-5 mx-auto sauce-details-content">
                <h3>Zutaten</h3>
                <p>
                  <strong>Hot Habanero Sauce</strong>
                </p>
                <p>Branntweinessig (Weingeistessig), Tomatenmark, Habanero, Salz, Olivenöl</p>
                <p><small>Enthält keine Zusatz- und Aromastoffe.<br>Laktose und glutenfrei.</small></p>
              </div>
              <div class="col-12 col-md-5 mx-auto sauce-details-content">
                <h3>Nährwertangaben</h3>
                <div class="row">
                  <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <p>&nbsp;</p>
                        </div>
                      <div class="col-4 text-right">
                        <p><strong>pro 100 g</strong></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Brennwert</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>262 KJ / 61 kcal</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Fett<br>- davon gesättigte Fettsäuren</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>0,4 g<br>0,16 g</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Kohlenhydrate<br>- davon Zucker</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>7,4 g<br>1,9 g</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Eiweiß</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>1,0 g</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Ballaststoffe</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>0,8 g</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <p>Salz</p>
                      </div>
                      <div class="col-4 text-right">
                        <p>6,0 g</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="sauce-barbecue" class="col-12 sauce-target">
        <div class="row">
          <div class="col-12 sauce-ingredients order-2 order-md-1">
            <div class="sauce-ingredients-container pos-2 vinegar">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/essig.svg" alt="Essig">
              <p>Essig</p>
            </div>
            <div class="sauce-ingredients-container pos-1-1 habanero">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/habanero-1.svg" alt="Habanero">
              <p>Habanero</p>
            </div>
            <!--<div class="sauce-ingredients-container pos-1-1 turmeric">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/kurkuma.svg" alt="Kurkuma">
              <p>Kurkuma</p>
            </div>-->
            <div class="sauce-ingredients-container pos-4 oil">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/oel.svg" alt="Öl">
              <p>Öl</p>
            </div>
            <div class="sauce-ingredients-container pos-4-1 tomato">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/tomate.svg" alt="Tomate">
              <p>Tomate</p>
            </div>
            <!--<div class="sauce-ingredients-container pos-3 cinnamon">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zimt.svg" alt="Zimt">
              <p>Zimt</p>
            </div>-->
            <div class="sauce-ingredients-container pos-3-1 onion">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zwiebel.svg" alt="Zwiebel">
              <p>Zwiebel</p>
            </div>
          </div>
          <div class="col-12 sauce-images">
            <div class="row">
              <div class="col-5 ml-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-bbq.png" alt="Hot Barbecue Sauce">
              </div>
              <div class="col-5 mr-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-bbq-klein.png" alt="Hot Barbecue Sauce">
              </div>
            </div>
          </div>

            <div class="col-12 sauce-details order-3 order-md-2">
                <div class="row">
                    <div class="col-12 col-md-5 mx-auto sauce-details-content">
                        <h3>Zutaten</h3>
                        <p>
                            <strong>Hot Barbecue Sauce</strong>
                        </p>
                        <p>Branntweinessig (Weingeistessig), Tomatenmark, Habanero, Zucker, Salz, Zwiebel, Knoblauch, Oregano, Barbecue Gew&uuml;rzmischung, Ingwer, Senf<strong>*</strong>, Olivenöl</p>
                        <p><small>Enthält keine Zusatz- und Aromastoffe.<br>Laktose und glutenfrei.<br><strong>*</strong>Enthält Senf.</small></p>
                    </div>
                    <div class="col-12 col-md-5 mx-auto sauce-details-content">
                        <h3>Nährwertangaben</h3>
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p><strong>pro 100 g</strong></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Brennwert</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>614 KJ / 145 kcal</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Fett<br>- davon gesättigte Fettsäuren</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>0,59 g<br>0,11 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Kohlenhydrate<br>- davon Zucker</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>27,8 g<br>26,2 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Eiweiß</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>1,73 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Ballaststoffe</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>1,9 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Salz</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>3,53 g</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div id="sauce-curry" class="col-12 sauce-target">
        <div class="row">
          <div class="col-12 sauce-ingredients order-2 order-md-1">
            <div class="sauce-ingredients-container pos-3 vinegar">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/essig.svg" alt="Essig">
              <p>Essig</p>
            </div>
            <!--<div class="sauce-ingredients-container pos-3-1 clove">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/gewuerznelken.svg" alt="Gewürznelken">
              <p>Gewürznelken</p>
            </div>-->
            <!--<div class="sauce-ingredients-container pos-4">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/habanero-1.svg" alt="Habanero">
              <p>Habanero</p>
            </div>-->
            <div class="sauce-ingredients-container pos-4 turmeric">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/kurkuma.svg" alt="Kurkuma">
              <p>Kurkuma</p>
            </div>
            <div class="sauce-ingredients-container pos-1 oil">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/oel.svg" alt="Öl">
              <p>Öl</p>
            </div>
            <div class="sauce-ingredients-container pos-1-1 tomato">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/tomate.svg" alt="Tomate">
              <p>Tomate</p>
            </div>
            <!--<div class="sauce-ingredients-container pos-2 cinnamon">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zimt.svg" alt="Zimt">
              <p>Zimt</p>
            </div>-->
            <div class="sauce-ingredients-container pos-2-1 onion">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zwiebel.svg" alt="Zwiebel">
              <p>Zwiebel</p>
            </div>
          </div>
          <div class="col-12 sauce-images">
            <div class="row">
              <div class="col-5 ml-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-curry.png" alt="Hot Curry Sauce">
              </div>
              <div class="col-5 mr-auto col-md-12 sauce-image">
                <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-curry-klein.png" alt="Hot Curry Sauce">
              </div>
            </div>
          </div>
            <div class="col-12 sauce-details order-3 order-md-2">
                <div class="row">
                    <div class="col-12 col-md-5 mx-auto sauce-details-content">
                        <h3>Zutaten</h3>
                        <p>
                            <strong>Hot Curry Sauce</strong>
                        </p>
                        <p>Branntweinessig (Weingeistessig), Tomatenmark, Habanero, Zucker, Salz, Zwiebel, Curry Gew&uuml;rzmischung, Knoblauch, Senf<strong>*</strong>, Kurkuma, Ingwer, Olivenöl</p>
                        <p><small>Enthält keine Zusatz- und Aromastoffe.<br>Laktose und glutenfrei.<br><strong>*</strong>Enthält Senf.</small></p>
                    </div>
                    <div class="col-12 col-md-5 mx-auto sauce-details-content">
                        <h3>Nährwertangaben</h3>
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p><strong>pro 100 g</strong></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Brennwert</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>571 KJ / 134 kcal</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Fett<br>- davon gesättigte Fettsäuren</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>0,52 g<br>0,09 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Kohlenhydrate<br>- davon Zucker</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>26,1 g<br>19,5 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Eiweiß</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>1,73 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Ballaststoffe</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>1,6 g</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p>Salz</p>
                                    </div>
                                    <div class="col-4 text-right">
                                        <p>3,4 g</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
        <div id="sauce-trinidad" class="col-12 sauce-target">
            <div class="row">
                <div class="col-12 sauce-ingredients order-2 order-md-1">
                    <div class="sauce-ingredients-container pos-3 vinegar">
                        <img src="<?php echo get_template_directory_uri(); ?>/svg/essig.svg" alt="Essig">
                        <p>Essig</p>
                    </div>
                    <!--<div class="sauce-ingredients-container pos-2-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/gewuerznelken.svg" alt="Gewürznelken">
              <p>Gewürznelken</p>
            </div>-->
                    <div class="sauce-ingredients-container pos-1-1 habanero">
                        <img src="<?php echo get_template_directory_uri(); ?>/svg/habanero-1.svg" alt="Habanero">
                        <p>Habanero</p>
                    </div>
                    <!--<div class="sauce-ingredients-container pos-1-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/kurkuma.svg" alt="Kurkuma">
              <p>Kurkuma</p>
            </div>-->
                    <div class="sauce-ingredients-container pos-4 oil">
                        <img src="<?php echo get_template_directory_uri(); ?>/svg/oel.svg" alt="Öl">
                        <p>Öl</p>
                    </div>
                    <div class="sauce-ingredients-container pos-2-1 tomato">
                        <img src="<?php echo get_template_directory_uri(); ?>/svg/tomate.svg" alt="Tomate">
                        <p>Tomate</p>
                    </div>
                    <!--<div class="sauce-ingredients-container pos-3 zimt">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zimt.svg" alt="Zimt">
              <p>Zimt</p>
            </div>-->
                    <!--<div class="sauce-ingredients-container pos-3-1">
              <img src="<?php echo get_template_directory_uri(); ?>/svg/zwiebel.svg" alt="Zwiebel">
              <p>Zwiebel</p>
            </div>-->
                </div>
                <div class="col-12 sauce-images">
                    <div class="row">
                        <div class="col-5 ml-auto col-md-12 sauce-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-trinidad.png" alt="Trinidad Sauce">
                        </div>
                        <div class="col-5 mr-auto col-md-12 sauce-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/akoba-trinidad-klein.png" alt="Trinidad Sauce">
                        </div>
                    </div>
                </div>
                <div class="col-12 sauce-details order-3 order-md-2">
                    <div class="row">
                        <div class="col-12 col-md-5 mx-auto sauce-details-content">
                            <h3>Zutaten</h3>
                            <p>
                                <strong>Trinidad Scorpion Sauce</strong>
                            </p>
                            <p>Branntweinessig (Weingeistessig), Tomatenmark, Habanero, Salz, Olivenöl</p>
                            <p><small>Enthält keine Zusatz- und Aromastoffe.<br>Laktose und glutenfrei.</small></p>
                        </div>
                        <div class="col-12 col-md-5 mx-auto sauce-details-content">
                            <h3>Nährwertangaben</h3>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <p>&nbsp;</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p><strong>pro 100 g</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Brennwert</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>175 KJ / 41 kcal</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Fett<br>- davon gesättigte Fettsäuren</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>0,77 g<br>0,19 g</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Kohlenhydrate<br>- davon Zucker</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>2,5 g<br>1,1 g</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Eiweiß</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>1,22 g</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Ballaststoffe</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>1,0 g</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <p>Salz</p>
                                        </div>
                                        <div class="col-4 text-right">
                                            <p>4,70 g</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
</section>

<section id="inspiration" data-section-name="inspiration">
  <div class="container-fluid">
    <h2 class="text-center">Inspiration</h2>
    <img src="<?php echo get_template_directory_uri(); ?>/img/inspiration-header.png" alt="Habanero Header">
    <div class="row no-gutters">
      <div class="col-sm-6 image-wrapper">
        <img src="<?php echo get_template_directory_uri(); ?>/img/doenerinspiration.png" alt="Inspiraton Nr.1">
      </div>
      <div class="col-sm-6 col-md-5 col-lg-4 mx-auto ml-sm-0 content-wrapper">
        <h3>Döner trifft AKOBA.</h3>
        <p>Schärfe und Döner - Besser geht's nicht!<br>Frisches Fleisch mit Salat, zwei Scheiben Tomaten, dazwischen Zwiebel, eine leckere Joghurtsauce und on top: AKOBA. Köstlich.</p>
      </div>
    </div>
    <div class="row no-gutters">
      <div class="col-sm-6 col-md-5 col-lg-4 mx-auto mr-sm-0 order-2 order-sm-1 text-right content-wrapper">
        <h3>Bella AKOBA</h3>
        <p>Spaghetti alla AKOBA. Das gen&uuml;ssliche Gericht lässt sich perfekt mit einer scharfen Tomatensoße kombinieren. Vielleicht noch mit Käse obendrauf. Einfach lecker. Oder wie der Italiener sagen w&uuml;rde: delizioso.</p>
      </div>
      <div class="col-sm-6 order-1 order-sm-2 image-wrapper">
        <img src="<?php echo get_template_directory_uri(); ?>/img/nudelinspiration.png" alt="Inspiraton Nr.2">
      </div>
    </div>
      <div class="row no-gutters">
          <div class="col-sm-6 image-wrapper">
              <img src="<?php echo get_template_directory_uri(); ?>/img/steakinspiration.png" alt="Inspiraton Nr.3">
          </div>
          <div class="col-sm-6 col-md-5 col-lg-4 mx-auto ml-sm-0 content-wrapper">
              <h3>Steak? AKOBA!</h3>
              <p>Neben dem saftig gegrilltem Fleisch darf AKOBA aufjedenfall nicht fehlen. Sie f&uuml;gt die fehlende Schärfe hinzu und macht das Steak somit komplett. Also eine feurige Angelegenheit.</p>
          </div>
      </div>
      <div class="row no-gutters">
          <div class="col-sm-6 col-md-5 col-lg-4 mx-auto mr-sm-0 order-2 order-sm-1 text-right content-wrapper">
              <h3>Akoba-Suppe</h3>
              <p>Eine gut gew&uuml;rzte Suppe gibt es nicht? - Doch gibt's! Ein Schuss AKOBA gibt der Suppe ein schmackhaftes etwas der jedermann gefallen wird. AKOBA macht jede Suppe wortwörtlich schärfer.</p>
              <p>Eine gut gew&uuml;rzte Suppe gibt es nicht? - Doch gibt's! Ein Schuss AKOBA gibt der Suppe ein schmackhaftes etwas der jedermann gefallen wird. AKOBA macht jede Suppe wortwörtlich schärfer.</p>
          </div>
          <div class="col-sm-6 order-1 order-sm-2 image-wrapper">
              <img src="<?php echo get_template_directory_uri(); ?>/img/suppeinspiration3.png" alt="Inspiraton Nr.4">
          </div>
      </div>
  </div>
</section>


<?php
get_footer();
