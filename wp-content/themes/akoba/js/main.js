// Classes
function Scrollify() {
  this.elems = {
    navLink: '.nav-link',
    header: '#header',
    footer: '#footer',
    sections: 'section',
    intro: '#intro',
    sauce: {
      section: '#sauce',
      triggers: '#sauce-triggers button',
      targets: '#sauce-targets .sauce-target',
      wrapper: '#sauce-targets',
      image: '.sauce-image',
      details: '.sauce-details'
    }
  },
  this.scrollOffset = 100,
  this.states = {
    navActive: 'active',
    minified: 'minified',
    visible: 'nav-visible',
    highlightFooter: 'overlay overlay-footer',
    sauce: {
      triggerActive: 'active',
      active: 'active',
      sectionActive: 'display-sauces'
    }
  },
  this.scrollNavigation = function() {
    var navTarget;

    jQuery(this.elems.navLink).on('touchstart click', function(e) {
      var scrollify = this;

      e.preventDefault();
      navTarget = jQuery(this).attr('href');
      jQuery('html, body').animate({
        scrollTop: jQuery(navTarget).offset().top
      }, 600, 'easeOutExpo', function() {
        window.location.hash = navTarget;
        // here put the close
          mobileNav.reset(mobileNav.elems.body)
      });
    });
  },
  this.getSectionsScrollTop = function() {
    var scrollTopSections = [],
        section;

    jQuery(this.elems.sections).each(function() {
      section = {
        top: Math.floor(jQuery(this).offset().top),
        bottom: Math.floor(jQuery(this).offset().top + jQuery(this).outerHeight() -1),
        hash: '#' + jQuery(this).attr('id')
      };

      scrollTopSections.push(section);
    });

    return scrollTopSections;
  },
  this.getElemScrollMiddle = function(elem) {
    var top = jQuery(elem).offset().top,
        height = jQuery(elem).outerHeight(),
        middle = height / 2,
        scrollMiddle = top + middle;

    return scrollMiddle;
  },
  this.getCurrentSection = function(scrollTop) {
    var scrollTopSections = this.getSectionsScrollTop();

    for (var i = 0; i < scrollTopSections.length; i++) {
      if (scrollTop >= scrollTopSections[i].top && scrollTop <= scrollTopSections[i].bottom) {
        return scrollTopSections[i].hash;
      }
    }
  },
  this.getVisibleSection = function(scrollTop) {
    var scrollBottom = scrollTop + jQuery(window).height(),
        scrollTopSections = this.getSectionsScrollTop();

    for (var i = 0; i < scrollTopSections.length; i++) {
      if (scrollBottom >= scrollTopSections[i].top && scrollBottom <= scrollTopSections[i].bottom) {
        return scrollTopSections[i].hash;
      }
    }
  },
  this.showHeader = function(scrollTop) {
    var offset = jQuery(this.elems.intro).outerHeight();

    if (scrollTop >= offset) {
      jQuery(this.elems.header).addClass(this.states.visible);
    } else {
      jQuery(this.elems.header).removeClass(this.states.visible);
    }
  },
  this.minifyHeader = function(scrollTop) {
    var offset = this.scrollOffset + jQuery(this.elems.intro).outerHeight();

    if (scrollTop >= offset) {
      jQuery(this.elems.header).addClass(this.states.minified);
    } else {
      jQuery(this.elems.header).removeClass(this.states.minified);
    }
  },
  this.toggleNav = function(scrollTop) {
    var scrollify = this,
        currentSection = this.getCurrentSection(scrollTop);

    jQuery(this.elems.navLink).each(function() {
      jQuery(this).parent().removeClass(scrollify.states.navActive);
    });
    jQuery(this.elems.navLink + '[href="' + currentSection + '"]').parent().addClass(this.states.navActive);
  },
  this.highlightFooter = function(scrollTop) {
    var scrollBottom = scrollTop + jQuery(window).height(),
        scrollMiddleFooter = this.getElemScrollMiddle(this.elems.footer);

    if (scrollBottom >= scrollMiddleFooter) {
      jQuery('body').addClass(this.states.highlightFooter);
    } else {
      jQuery('body').removeClass(this.states.highlightFooter);
    }
  },
  this.initSauces = function(scrollTop) {
    var visibleSection = this.getVisibleSection(scrollTop),
        height;

    if (visibleSection == this.elems.sauce.section) {
      height = jQuery(this.elems.sauce.targets).find(this.elems.sauce.image).outerHeight() + jQuery(this.elems.sauce.targets).find(this.elems.sauce.details).outerHeight();

      // this.resetSauces();
      jQuery(this.elems.sauce.section).addClass(this.states.sauce.sectionActive);
      jQuery(this.elems.sauce.wrapper).css({
        'height' : height
      });
    }
  },
  this.toggleSauces = function() {
    var scrollify = this,
        target,
        height;

    jQuery(this.elems.sauce.triggers).on('touchstart click', function() {
      target = jQuery(this).attr('data-target');
      height = jQuery(target).find(scrollify.elems.sauce.image).outerHeight() + jQuery(target).find(scrollify.elems.sauce.details).outerHeight();

      // Set triggers active/inactive
      jQuery(scrollify.elems.sauce.triggers).each(function() {
        jQuery(this).removeClass(scrollify.states.sauce.triggerActive);
      });
      jQuery(this).addClass(scrollify.states.sauce.triggerActive);

      // Set targets active/inactive
      jQuery(scrollify.elems.sauce.targets).each(function() {
        jQuery(this).removeClass(scrollify.states.sauce.active);
      }),
      jQuery(target).addClass(scrollify.states.sauce.active);
      jQuery(scrollify.elems.sauce.wrapper).css({
        'height' : height
      });
    })
  },
  this.resetSauces = function() {
    jQuery(this.elems.sauce.triggers).first().addClass(this.states.sauce.triggerActive);
    jQuery(this.elems.sauce.targets).first().addClass(this.states.sauce.active);
  },
  this.init = function() {
    var scrollify = this,
        scrollTop = jQuery(window).scrollTop();

    this.scrollNavigation();
    this.showHeader(scrollTop);
    this.minifyHeader(scrollTop);
    this.toggleNav(scrollTop);
    this.highlightFooter(scrollTop);
    this.initSauces(scrollTop);
    this.toggleSauces();


    jQuery(window).on('scroll resize', function() {
      scrollTop = jQuery(window).scrollTop();

      scrollify.showHeader(scrollTop);
      scrollify.minifyHeader(scrollTop);
      scrollify.toggleNav(scrollTop);
      scrollify.highlightFooter(scrollTop);
      scrollify.initSauces(scrollTop);
    });
  }
}

function MobileNav() {
  this.elems = {
    nav: '#header .nav',
    mobile: '#mobileNav',
    trigger: '[data-toggle="toggle"]',
    body: 'body'
  },
  this.states = {
    active: 'mobile-nav',
    overlay: 'overlay-2'
  },
  this.dupe = function() {
    var nav = jQuery(this.elems.nav).clone();

    jQuery(this.elems.mobile).append(nav);
  },
  this.toggle = function(elem) {
    jQuery(elem).toggleClass(this.states.active);
    jQuery(elem).toggleClass(this.states.overlay);
  },
  this.reset = function(elem) {
    jQuery(elem).removeClass(this.states.active);
    jQuery(elem).removeClass(this.states.overlay);
  }
  this.init = function() {
    var mobileNav = this,
        trigger,
        wrapper;

    // Duplicate desktop nav
    this.dupe();

    // Toggle duplicated on trigger click
    jQuery(document).on('ontouchend click', function(event) {
      trigger = jQuery(event.target).closest(mobileNav.elems.trigger);
      wrapper = jQuery(event.target).closest(mobileNav.elems.mobile);

      if (trigger.length) {
        mobileNav.toggle(mobileNav.elems.body);
      }

      if (!trigger.length && !wrapper.length) {
        mobileNav.reset(mobileNav.elems.body);
      }
    });
  }
}

function CookieModal() {
    this.elems = {
        link: 'a.cc-link',
        privacy: '#privacy'
    },
    this.changeLink = function() {
        var privacy = this.elems.privacy;
        jQuery('body').on( 'click', this.elems.link, function( e ) {
            e.preventDefault();
            jQuery(privacy).modal('show');
        });
    },
    this.init = function() {
      this.changeLink();
    }
}

// Objects
var scrollify = new Scrollify();
var mobileNav = new MobileNav();
var cookieModal = new CookieModal();

// DOM
jQuery(document).ready(function() {
  mobileNav.init();
  scrollify.init();
  cookieModal.init();
});
